#include <iostream> 

#include <chrono> 
#include <thread> 

using namespace std; 

void afisareBara(int progres, int total) { // definim o functie numita afisareBara care primeste doua argumente: progresul curent și totalul.
    const int latime = 50; // definim latimea barii de progres ca o constantă de 50 de caractere.
    float procent = static_cast<float>(progres) / total; // calculam procentajul de progres.
    int numBara = static_cast<int>(procent * latime); // calculam numărul de caractere "=" pentru bara de progres.

    cout << "["; // afism un '[' pentru începutul barei de progres.
    for (int i = 0; i < latime; ++i) { // incepem o bucla care rulează de la 0 la latimea barei de progres.
        cout << (i < numBara ? "=" : " "); // afisam caracterele "=" sau spatii în functie de progresul curent.
    }
    cout << "] " << int(procent * 100.0) << "%" << "\r"; // afisam progresul sub forma "[=====     ] 50%" si ne intoarcem la inceputul liniei cu "\r".
    cout.flush(); // curatamm buffer-ul de iesire pentru a asigura afișarea imediată.

}

int main() {
    const int iteratii = 100; // definim numărul total de iteratii pentru bara de progres.

    for (int i = 0; i <= iteratii; ++i) { // incepem o bucla care rulează de la 0 la numărul total de iterații.
        afisareBara(i, iteratii); // afisam bara de progres cu progresul curent și numărul total de iterații.
        this_thread::sleep_for(chrono::milliseconds(50)); // asteptam pentru a simula o operatie care durează puțin timp 50 de milisecunde în acest caz.
    }

    cout << "\nOperatia a fost finalizata!\n"; // afisam mesajul de finalizare a operatiei.

    return 0; // returnam valoarea 0 pentru a indica succesul programului.
}