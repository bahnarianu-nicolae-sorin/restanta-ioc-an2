**Acest program facut in C++ este o bara de progres pe care o vedem foarte des in majoritatea aplicatiilor pe care le folosim.
Are rolul de a arata cat de mult a progresat o operatie sau sarcina si cat mai dureaza pana la finalizare.
De asemenea, progress bar-urile mai sunt folosite pentru a permite aplicatiei sau software-ului sa incarce resursele din back-end.

Pentru acest program am scris singur codul de baza, ca mai apoi sa il bag in Chat GPT sa ma ajute sa rezolv erorile si eventual
sa il fac mai compact si modular si mai eficient.


La rularea programului, apare o linie de comanda cu caracterele: "=", avem si un procentaj care apare in aceasta operatie.
Programul simuleaza efectuarea unei operatii precum descarcarea unui fisier sau incarcarea unui fisier.

Acest proiect este creat de Bahnarianu Nicolae-Sorin, Anul 2, Grupa 201 pentru restanta in anul 2 la Interfete Om-Calculator (IOC)**
